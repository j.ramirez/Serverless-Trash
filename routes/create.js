'use strict';


const uuid = require('uuid');
const AWS = require('aws-sdk');
const dynamoDb = new AWS.DynamoDB.DocumentClient();


module.exports.create = (event, context, callback) => {
  const data = JSON.parse(event.body);

  const params = {
    TableName: `${process.env.DYNAMODB_TABLE}-routes`,
    Item: {
      id: uuid.v1(),
      name: data.name,
      date: data.date,
      clients: data.clients,
      crew: data.crew
    },
  };

  dynamoDb.put(params, (error) => {
    const response = { statusCode: 200, body: JSON.stringify(params.Item) };
    callback(null, response);
  });
};
