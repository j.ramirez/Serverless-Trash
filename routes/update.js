'use strict';


const AWS = require('aws-sdk');
const dynamoDb = new AWS.DynamoDB.DocumentClient();


module.exports.update = (event, context, callback) => {
  const data = JSON.parse(event.body);


  const params = {
    TableName: `${process.env.DYNAMODB_TABLE}-routes`,
    Key: { id: event.pathParameters.id },
    ExpressionAttributeValues: {
      ':name': data.name,
      ':day': data.day,
      ':crew': data.crew,
      ':clients': data.clients
    },
    UpdateExpression: 'SET name = :name, day = :day, clients = :clients, crew = :crew',
    ReturnValues: 'ALL_NEW',
  };

  dynamoDb.update(params, (error, result) => {
    const response = { statusCode: 200, body: JSON.stringify(result.Attributes) };
    callback(null, response);
  });
};
