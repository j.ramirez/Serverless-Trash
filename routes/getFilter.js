'use strict';


const AWS = require('aws-sdk');
const Lodash = require('lodash');
const dynamoDb = new AWS.DynamoDB.DocumentClient();
const params = {
  TableName: `${process.env.DYNAMODB_TABLE}-routes`,
};


module.exports.get = (event, context, callback) => {
  dynamoDb.scan(params, (error, result) => {
    const objBody = result && result.Items ? result.Items : [];
    const data = objBody.filter((objRoute) => {
      const objCrew = Lodash.get(objRoute, ['crew', 0], {});
      return objRoute.date == event.pathParameters.day && objCrew.id == event.pathParameters.crew
    });
    const response = { statusCode: 200, body: JSON.stringify(data) };
    callback(null, response);
  });
};
