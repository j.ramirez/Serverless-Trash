'use strict';


const uuid = require('uuid');
const AWS = require('aws-sdk');
const dynamoDb = new AWS.DynamoDB.DocumentClient();


module.exports.create = (event, context, callback) => {
  const data = JSON.parse(event.body);

  const params = {
    TableName: `${process.env.DYNAMODB_TABLE}-complains`,
    Item: {
      id: uuid.v1(),
      client: data.client,
      text: data.text
    },
  };

  dynamoDb.put(params, (error) => {
    const response = { statusCode: 200, body: JSON.stringify(params.Item) };
    callback(null, response);
  });
};
