'use strict';


const AWS = require('aws-sdk');
const dynamoDb = new AWS.DynamoDB.DocumentClient();


module.exports.delete = (event, context, callback) => {
  const params = {
    TableName: `${process.env.DYNAMODB_TABLE}-complains`,
    Key: { id: event.pathParameters.id }
  };

  dynamoDb.delete(params, (error) => {
    const response = { statusCode: 200, body: JSON.stringify({}) };
    callback(null, response);
  });
};
