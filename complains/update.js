'use strict';


const AWS = require('aws-sdk');
const dynamoDb = new AWS.DynamoDB.DocumentClient();


module.exports.update = (event, context, callback) => {
  const data = JSON.parse(event.body);


  const params = {
    TableName: `${process.env.DYNAMODB_TABLE}-complains`,
    Key: {
      id: event.pathParameters.id,
    },
    ExpressionAttributeValues: {
      ':client': data.client,
      ':text': data.text
    },
    UpdateExpression: 'SET client = :client, text = :text',
    ReturnValues: 'ALL_NEW',
  };

  dynamoDb.update(params, (error, result) => {
    const response = { statusCode: 200, body: JSON.stringify(result.Attributes) };
    callback(null, response);
  });
};
