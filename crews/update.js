'use strict';


const AWS = require('aws-sdk');
const dynamoDb = new AWS.DynamoDB.DocumentClient();


module.exports.update = (event, context, callback) => {
  const data = JSON.parse(event.body);


  const params = {
    TableName: `${process.env.DYNAMODB_TABLE}-crews`,
    Key: { id: event.pathParameters.id },
    ExpressionAttributeValues: {
      ':users': data.users,
      ':city': data.city
    },
    UpdateExpression: 'SET users = :users, city = :city',
    ReturnValues: 'ALL_NEW',
  };

  dynamoDb.update(params, (error, result) => {
    const response = { statusCode: 200, body: JSON.stringify(result.Attributes) };
    callback(null, response);
  });
};
