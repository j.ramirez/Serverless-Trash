'use strict';


const uuid = require('uuid');
const AWS = require('aws-sdk');
const BcryptX = require('./bcrypt-x');
const dynamoDb = new AWS.DynamoDB.DocumentClient();


module.exports.create = (event, context, callback) => {
  const data = JSON.parse(event.body);

  BcryptX.hashPassword(data.password).then((strHash) => {
    const params = {
      TableName: `${process.env.DYNAMODB_TABLE}-users`,
      Item: {
        id: uuid.v1(),
        _id: data._id,
        email: data.email,
        password: strHash,
        firstname: data.firstname,
        lastname: data.lastname,
        street: data.street,
        number: data.number,
        city: data.city,
        sector: data.sector,
        type: data.type
      },
    };

    dynamoDb.put(params, (error) => {
      const response = { statusCode: 200, body: JSON.stringify(params.Item) };
      callback(null, response);
    });
  });
};
