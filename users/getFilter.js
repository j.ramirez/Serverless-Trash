'use strict';


const AWS = require('aws-sdk');
const dynamoDb = new AWS.DynamoDB.DocumentClient();
const params = {
  TableName: `${process.env.DYNAMODB_TABLE}-users`,
};


module.exports.get = (event, context, callback) => {
  dynamoDb.scan(params, (error, result) => {
    const objBody = result && result.Items ? result.Items : [];
    const data = objBody.filter((objUser) => {
      return objUser.city == event.pathParameters.city
    });
    const response = { statusCode: 200, body: JSON.stringify(data) };
    callback(null, response);
  });
};
