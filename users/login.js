'use strict';


const AWS = require('aws-sdk');
const dynamoDb = new AWS.DynamoDB.DocumentClient();
const BcryptX = require('./bcrypt-x');
const Lodash = require('lodash');


module.exports.login = (event, context, callback) => {
  const data = JSON.parse(event.body);

  const params = {
    TableName: `${process.env.DYNAMODB_TABLE}-users`,
    ExpressionAttributeValues: {
      ":email": data.email
    },
    FilterExpression: "contains (email, :email)"
  };

  dynamoDb.scan(params, (error, result) => {
    const listUsers = result && result.Items ? result.Items : [];
    const objUser = Lodash.get(listUsers, [0], {});
    BcryptX.isValidPassword(data.password, objUser.password).then(() => {
      const response = { statusCode: 200, body: JSON.stringify(objUser) };
      callback(null, response);
    }).catch(() => {
      const response = { statusCode: 401, body: JSON.stringify({}) };
      callback(null, response);
    });
  });
};
