'use strict';


const AWS = require('aws-sdk');
const dynamoDb = new AWS.DynamoDB.DocumentClient();
const params = {
  TableName: `${process.env.DYNAMODB_TABLE}-users`,
};


module.exports.list = (event, context, callback) => {
  dynamoDb.scan(params, (error, result) => {
    const objBody = result && result.Items ? result.Items : [];
    const response = { statusCode: 200, body: JSON.stringify(objBody) };
    callback(null, response);
  });
};
