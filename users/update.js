'use strict';


const AWS = require('aws-sdk');
const BcryptX = require('./bcrypt-x');
const dynamoDb = new AWS.DynamoDB.DocumentClient();


module.exports.update = (event, context, callback) => {
  const data = JSON.parse(event.body);


  const params = {
    TableName: `${process.env.DYNAMODB_TABLE}-users`,
    Key: {
      id: event.pathParameters.id,
    },
    ExpressionAttributeValues: {
      ':firstname': data.firstname,
      ':lastname': data.lastname,
      ':password': BcryptX.hashPassword(data.password),
      ':email': data.email,
      ':street': data.street,
      ':number': data.number,
      ':sector': data.sector,
      ':city': data.city,
      ':type': data.type
    },
    UpdateExpression: 'SET firstname = :firstname, lastname = :lastname, street = :street, number = :number, sector = :sector, city = :city, type = :type, email = :email, password = :password',
    ReturnValues: 'ALL_NEW',
  };

  dynamoDb.update(params, (error, result) => {
    const response = { statusCode: 200, body: JSON.stringify(result.Attributes) };
    callback(null, response);
  });
};
